## Usage

Set the attribute `data-tooltip="true"` to your necessary HTML Element.

## Options

- **data-tooltip="true"**  
    Set the Tooltip

- **data-tooltip-content="<b>Yay!</b> My awesome tooltip"**  
    Set the tooltips content. HTML is allowed
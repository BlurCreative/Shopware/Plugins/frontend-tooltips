;(function($, window) {
    'use strict';

	//var $window = $(window);
	var $body = $('body');

    $.plugin('bcTooltip', {

        defaults: {
			position: 'bottom',
			spacing: 10,
			fading: true,
			additional_class: ''
        },

        /**
         * Default plugin initialisation function.
         * Sets all needed properties, adds classes and registers all needed event listeners.
         *
         * @public
         * @method init
         */
        init: function() {
			var me = this;

			me.device = null;
			
			if ( ( typeof window.orientation !== "undefined" ) || ( navigator.userAgent.indexOf( 'IEMobile' ) !== -1 ) ) {
				// on mobile
				me.device = 'mobile';
			} else {
				// on desktop
				me.device = 'desktop';
			}
			
			me.disabled = me.$el.prop( 'disabled' );

			if ( me.disabled === true) {
				me.check_disabled_element();

				me.$el.click( function() {
					return false;
				});
			}

            me.applyDataAttributes();
			
			me.tooltip_content = me.$el.data( 'tooltip-content' );
			
			me.tooltip_container = $( '<div>' , {
				'class': 'tooltip-container tooltip-position-' + me.opts.position + ( me.opts.additional_class ? ' ' + me.opts.additional_class : '' ),
				'html': me.tooltip_content
			});
			
			me.registerEvents();
			
		},
		
        /**
         * Subscribes all required events.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function() {
			var me = this;
	
			if ( me.device === 'desktop' ) {
				me._on( me.$el , 'mouseenter mouseleave' , $.proxy( me.hover_tooltip_link , me ) );
			}
			if ( me.device === 'mobile' ) {
				me._on( me.$el , 'pointerdown pointerleave' , $.proxy( me.enter_tooltip_link , me ) );
				me._on( 'body' , 'pointerdown' , $.proxy( me.enter_body , me ) );
			}
				
			me._on( window , 'scroll' , $.proxy( me.remove_tooltip , me ) );	
        },
		
        /**
         * Subscribes all required events.
         *
         * @public
         * @method registerEvents
         */
        check_disabled_element: function() {
			var me = this;

			me.$el.prop( 'disabled' , false );
			me.disabled = false;
			
			
        },
		
        hover_tooltip_link: function( event ) {
			var me = this,
				tooltip_position = me.opts.position,
				me_pos_x = me.$el.offset().left,
				me_pos_y = null,
				me_height = me.$el.outerHeight( false ),
				me_width = me.$el.outerWidth( false ),
				tooltip_pos_x = null,
				tooltip_pos_y = null,
				tooltip_width = 0;
				
			$( '.tooltip-container' ).remove();
			
			me.tooltip_container.appendTo( 'body' );
			
			me_pos_y = me.$el.offset().top - $(document).scrollTop();
			tooltip_width = me.tooltip_container.outerWidth( false );
			
			/* if ( tooltip_position === 'bottom' ) { */
				tooltip_width = tooltip_width / 2;
				me_pos_x = me_pos_x + ( me_width / 2 );
				
				tooltip_pos_y = me_pos_y + me_height + me.opts.spacing;
				tooltip_pos_x = me_pos_x - tooltip_width;
			/* } */
			
			me.tooltip_container.css( {top: tooltip_pos_y, left: tooltip_pos_x} );
			
			if ( event.type === 'mouseleave' ) {
				me.remove_tooltip();
			}
        },
		
        enter_tooltip_link: function( event ) {
			var me = this,
				tooltip_position = me.opts.position,
				me_pos_x = me.$el.offset().left,
				me_pos_y = null,
				me_height = me.$el.outerHeight( false ),
				me_width = me.$el.outerWidth( false ),
				tooltip_pos_x = null,
				tooltip_pos_y = null,
				tooltip_width = 0;
			
			if ( event.type === 'pointerleave' ) {
			
				$( '.tooltip-container' ).remove();			
				me.tooltip_container.appendTo( 'body' );
				
				me_pos_y = me.$el.offset().top - $(document).scrollTop();
				tooltip_width = me.tooltip_container.outerWidth( false );
				
				/* if ( tooltip_position === 'bottom' ) { */
					tooltip_width = tooltip_width / 2;
					me_pos_x = me_pos_x + ( me_width / 2 );
					
					tooltip_pos_y = me_pos_y + me_height + me.opts.spacing;
					tooltip_pos_x = me_pos_x - tooltip_width;
				/* } */
				
				me.tooltip_container.css( {top: tooltip_pos_y, left: tooltip_pos_x} );
				
				return false;
				
			}
		
			
			
			if ( event.type === 'mouseleave' ) {
				me.remove_tooltip();
			}
        },
		
        enter_body: function( event ) {
			var me = this;
			
            if ( !$( event.target ).parents( me.$el ).is( me.$el ) || !$( event.target ).is( me.$el ) ) {
				me.remove_tooltip();
            }
			
			

		},

        remove_tooltip: function() {
			var me = this;
			
			me.tooltip_container.remove();
        },
    });
}(jQuery, window));